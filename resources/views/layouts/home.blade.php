<!DOCTYPE html>
<html lang="en">
<head>
	<title>InstaNote</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<script type="text/javascript" src="/js/app.js"></script>

</head>
<body style="background: white">
	<div class="container">
		<nav class="navbar navbar-default" style="border: 0px">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/division" style="font-weight: bold; font-size: 24px">InstaNote</a>
				</div>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="/logout" style="font-size: 16px">Sign In</a></li>
				</ul>
			</div>
		</nav>
	</div>

	@yield('content')
</body>
</html>